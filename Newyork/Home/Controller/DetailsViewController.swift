import UIKit

class DetailsViewController: UIViewController {
    
    var article:Article?
    
    var img:UIImageView?
    
    @IBOutlet weak var detailsImg: DesignableImageView!
    @IBOutlet weak var abstract: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Get last image
        if let imgUrl = article?.media.last?.mediaMetadata.last?.url
        {
            detailsImg.loadFromUrl(url_str: imgUrl)
        }
        abstract.text = article?.abstract
    }
    
    
}
