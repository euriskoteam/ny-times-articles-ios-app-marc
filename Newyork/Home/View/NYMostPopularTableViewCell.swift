import UIKit

class NYMostPopularTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var byline: UILabel!
    @IBOutlet weak var publishedDate: UILabel!
    
    @IBOutlet weak var nyImage: DesignableImageView!
    
    
    var article :Article? {
        didSet {
            title.text = article?.title
            byline.text = article?.byline
            if let imgUrl = article?.media.first?.mediaMetadata.first?.url
            {
                nyImage.loadFromUrl(url_str: imgUrl)
            }
            
            publishedDate.text = article?.published_date
            publishedDate.set(image: UIImage(named: "calendar.png"), with: publishedDate.text ?? "")
        }
    }
    
}

