import UIKit

struct Sections:Codable
{
    let status:String?
    let results:[Article]
}

struct SectionsViewModel {
    let status:Bool?
    
    init(section: Sections) {
        if(section.status?.lowercased() == "ok")
        {
            status = true
        }
        else
        {
            status = false
        }
    }
}

struct Article:Codable
{
    let title:String?
    let byline:String?
    let abstract:String?
    let published_date:String?
    let media:[Media]
}

struct Media:Codable
{
    let mediaMetadata:[MediaMetadata]
    
    private enum CodingKeys: String, CodingKey {
        case mediaMetadata = "media-metadata"
    }
}

struct MediaMetadata:Codable
{
    let url:String?
}
