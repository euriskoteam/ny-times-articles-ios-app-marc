import UIKit

public let KEY = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
public let PERIOD = "1"
public let MAIN_URL = "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/\(PERIOD).json?api-key=\(KEY)"
public let spinner = UIActivityIndicatorView(style: .gray)

public enum StoryBoard {
    static let Home = UIStoryboard.init(name: "Main", bundle: nil)
    
    enum controller {
        static let HomeVC = "Home"
        static let DetailsVC   =   "Details"
    }
}


public enum APIError
{
    static let noConnection = "No internet connection"
    static let unkwown = "Something went wrong,try again later"
}
