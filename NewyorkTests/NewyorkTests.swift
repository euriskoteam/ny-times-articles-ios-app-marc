
import XCTest
@testable import Newyork

class NewyorkTests: XCTestCase {

    var key:String?
    var periods:[String]?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        key = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
        periods = ["1","7","30"]
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        key = nil
        periods = nil
    }


    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testKey()
    {
        XCTAssertEqual(key,KEY)
    }

    func testPeriod()
    {
        
        XCTAssert(periods?.contains(PERIOD) ?? false)
    }
    
}
